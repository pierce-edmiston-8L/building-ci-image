# Building a CI image on CI

This repo demonstrates how to use GitLab's CI/CD tool to build
a custom CI image and cache project dependencies in order to
speed up builds.