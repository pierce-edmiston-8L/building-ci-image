#!/bin/sh
TAG=registry.gitlab.com/pierce-edmiston-8l/building-ci-image
docker login registry.gitlab.com
docker build --tag $TAG .
docker push $TAG