from typing import Optional


def main(name: Optional[str] = None) -> str:
    name = name or "World"
    return f"Hello, {name}!"
