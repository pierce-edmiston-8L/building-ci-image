from app.main import main


def test_app():
    assert main() == "Hello, World!"


def test_app_with_arg():
    assert main("name") == "Hello, name!"
